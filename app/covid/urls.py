"""covid URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.urls import path
from django.contrib import admin
from printData import views as printViews
from primaryData import views as primaryViews

urlpatterns = [
    path('admin/', admin.site.urls),
    path('home/', printViews.Home.as_view(), name="Home"),
    path('fetch/', primaryViews.Fetch.as_view(), name="Fetch"),
    path('about/', printViews.About.as_view(), name="About"),
    path('sources/', printViews.Sources.as_view(), name="Sources"),
]
