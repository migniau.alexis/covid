from django.db import models

class Vaccin(models.Model):
    name = models.CharField(max_length=50)

class Variant(models.Model):
    code = models.CharField(max_length=50)
    name = models.CharField(max_length=50)

class Molecule(models.Model):
    name = models.CharField(max_length=255)

# Create your models here.
class Article(models.Model):
    title = models.CharField(max_length=255)
    pubmed_id = models.IntegerField(unique=True)
    lang = models.CharField(max_length=3)
    date = models.DateField()
    country = models.CharField(max_length=255)
    variants = models.ManyToManyField(Variant)
    molecules = models.ManyToManyField(Molecule)
    vaccins = models.ManyToManyField(Vaccin)