from django.contrib import admin
from secondaryData.models import Variant, Molecule, Article, Vaccin

admin.site.register(Variant)
admin.site.register(Molecule)
admin.site.register(Article)
admin.site.register(Vaccin)