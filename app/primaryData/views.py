from django.http import JsonResponse
from django.views.generic.edit import CreateView
from Bio import Entrez
import datetime
from secondaryData.models import Article, Molecule, Vaccin, Variant

LIMIT = '5000'

# Create your views here.
class Fetch(CreateView):
    def get(self, request):
        # Get articles
        Entrez.email = "migniau.alexis@gmail.com"
        handleSearch = Entrez.esearch(db='pubmed',term='covid', retmax=LIMIT)
        ids = Entrez.read(handleSearch)
        handleFetch = Entrez.efetch(db='pubmed',id=ids['IdList'],rettype = 'abstract', retmod = 'xml')
        articles = Entrez.read(handleFetch)

        # Liste des variants
        list_v = Variant.objects.all()
        # Liste des  molecules
        list_m = Molecule.objects.all()
        # Liste des vaccins
        list_a = Vaccin.objects.all()

        for a in articles['PubmedArticle']:
            try:
                # Get useful data for each articles
                pmid = int(a['MedlineCitation']['PMID'])
                title = a['MedlineCitation']['Article']['ArticleTitle']
                lang = a['MedlineCitation']['Article']['Language'][0]
                date_string = a['MedlineCitation']['DateRevised']
                date = datetime.date(int(date_string['Year']),int(date_string['Month']),int(date_string['Day']))

                country = a['MedlineCitation']['MedlineJournalInfo']['Country']

                if(len(a['MedlineCitation']['Article']['Abstract']['AbstractText']) > 1):
                    text = '\n'.join(a['MedlineCitation']['Article']['Abstract']['AbstractText'])
                else:
                    text = a['MedlineCitation']['Article']['Abstract']['AbstractText'][0]

                # Save article to BDD
                article = Article(title=title, date=date,lang=lang,country=country,pubmed_id=pmid)
                article.save()

                # Recherche de variant
                for v in list_v:
                    if(text.find(v.code) != -1):
                        article.variants.add(v)
        
                # Recherche de molécule
                for m in list_m:
                    if(text.lower().find(m.name.lower()) != -1):
                        article.molecules.add(m)

                # Recherche de vaccin
                for a in list_a:
                    if(text.lower().find(a.name.lower()) != -1):
                        article.vaccins.add(a)
                        print("\n")
                        print(a)

            except Exception as e:
                pass

        return JsonResponse({"success": True})