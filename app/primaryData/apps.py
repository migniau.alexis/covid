from django.apps import AppConfig


class PrimarydataConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'primaryData'
