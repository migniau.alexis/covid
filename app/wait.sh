#!/bin/sh

while ! ./manage.py sqlflush > /dev/null 2>&1 ; do
    echo "Attente du serveur SQL"
    sleep 3
done

python manage.py runserver 0.0.0.0:8000