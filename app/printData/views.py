from django.shortcuts import render
from django.views.generic.edit import CreateView
from secondaryData.models import Vaccin, Variant, Molecule
from django.db.models import Count
from pprint import pprint

# Create your views here.
class Home(CreateView):
    def get(self, request):
        variants = Variant.objects.annotate(total=Count('article'))
        molecules = Molecule.objects.annotate(total=Count('article'))
        vaccins = Vaccin.objects.annotate(total=Count('article'))
        
        # Extraction des résultat pour les utiliser plus facilement dans HighCharts
        variants_cat = list()
        variants_res = list()

        for v in variants:
            variants_cat.append(v.name + ' (' + v.code + ')')
            variants_res.append(v.total)

        molecules_cat = list()
        molecules_res = list()

        for m in molecules:
            molecules_cat.append(m.name)
            molecules_res.append(m.total)

        vaccins_cat = list()
        vaccins_res = list()

        for v in vaccins:
            vaccins_cat.append(v.name)
            vaccins_res.append(v.total)

        return render(request, 'home.html', {
            'variants_cat' : variants_cat,
            'variants_res' : variants_res,
            'molecules_cat' : molecules_cat,
            'molecules_res' : molecules_res,
            'vaccins_cat' : vaccins_cat,
            'vaccins_res' : vaccins_res
        })

class About(CreateView):
    def get(self, request):
        return render(request, 'about.html')

class Sources(CreateView):
    def get(self, request):
        return render(request, 'sources.html')