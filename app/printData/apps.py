from django.apps import AppConfig


class PrintdataConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'printData'
