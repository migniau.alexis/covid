# Projet Django

## Prérequis
 - [Docker](https://docs.docker.com/engine/install/debian/#install-using-the-repository)
 - [Docker-compose](https://docs.docker.com/compose/install/#install-compose-on-linux-systems)

## Installation

- Copier le fichier .env.example en .env et adapter les valeurs si besoin

- Lancement de l'application :

```bash
docker-compose up -d
```

- Lors de la première execution, il faut lancer les migrations via la commande suivante :

```bash
docker-compose exec app python manage.py migrate
```

Pour créer un compte admin pour django, il faut faire la commande suivante :

```bash
docker-compose exec app python manage.py createsuperuser
```

## Services accessibles

_Si les valeurs du fichiers .env sont inchangés_

- [Application](http://localhost:8000/home/)
- [Administration](http://localhost:8000/admin/)

Contributeurs :
- Alexis Migniau
- Mohamed Lemine Diakhite
- Alexis Grégoire